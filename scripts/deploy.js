const FtpDeploy = require('ftp-deploy');
const ftp = new FtpDeploy();

ftp.deploy({
    host: process.env.FTP_HOST,
    user: process.env.FTP_USER,
    password: process.env.FTP_PASS,
    port: 21,
    localRoot: 'build',
    remoteRoot: './',
    include: ['*'],
    exclude: ['*.dev.js'],
    deleteRemote: true,
    forcePasv: true,
})
    .then(res => {
        console.log('Successfully uploaded:');
        res.map(item => console.log(item));
    })
    .catch(err => console.error(err));