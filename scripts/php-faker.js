const request = require('request');
const app = require('express')();
const port = process.env.PORT || 5000;

app.get('/curl.php', (req, res) => {
    request(req.query.u, (error, response, body) => {
        if (error) {
            res.statusCode = 400;
            res.send('URL is not valid');
            console.log(error);
        } else if (response) {
            res.send(body);
        }
    });
});

const server = app.listen(port, () => {
    const address = server.address();
    let host = address.address;

    if (host === '::' || host === '127.0.0.1') {
        host = 'localhost';
    } else if (address.family.toLowerCase() === 'ipv6') {
        host = `[${host}]`;
    }

    console.log(`PHP faker app listening at http://${host}:${address.port}`);
});