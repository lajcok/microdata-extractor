import {filter} from "./filter";

test('Basic functionality', () => {

    interface IMyType {
        a: string,
        b: number,
    }

    const obj: IMyType = {
        a: 'a',
        b: 2,
    };

    const res = filter(obj, (value, key) => {
        if(key === 'a' && typeof value === 'string') {
            return true;
        }
        if(key === 'b' && typeof value === 'number') {
            return false;
        }
        return fail(`Given property ${key} was not expected to be of ${typeof value} type`);
    });

    expect(res).toMatchObject({
        a: 'a',
    });

});

test('Static in Object', () => {

    interface IMyType {
        a: string,
        b: number,
    }

    const obj: IMyType = {
        a: 'a',
        b: 2,
    };

    const res = Object.filterer(obj, (value, key) => {
        if(key === 'a' && typeof value === 'string') {
            return true;
        }
        if(key === 'b' && typeof value === 'number') {
            return false;
        }
        return fail(`Given property ${key} was not expected to be of ${typeof value} type`);
    });

    expect(res).toMatchObject({
        a: 'a',
    });

});

test('Prototype of Object', () => {

    interface IMyType {
        a: string,
        b: number,
    }

    const obj: IMyType = {
        a: 'a',
        b: 2,
    };

    const res = obj.filterer<typeof obj>((value, key) => {
        if(key === 'a' && typeof value === 'string') {
            return true;
        }
        if(key === 'b' && typeof value === 'number') {
            return false;
        }
        return fail(`Given property ${key} was not expected to be of ${typeof value} type`);
    });

    expect(res).toMatchObject({
        a: 'a',
    });

});