/// <reference types="./map" />

/**
 * Maps all own properties to a function and
 * creates new object accordingly
 * *
 * @param object Input object
 * @param callback Mapper callback function
 */
export function map<
    T extends object,
    M extends Mapper<T> = Mapper<T>,
>(
    object: T,
    callback: M,
): Mapped<T, M> {
    const result: any = {};
    for (const key in object) {
        if (object.hasOwnProperty(key)) {
            result[key] = callback(object[key], key, object);
        }
    }
    return result;
}

Object.defineProperty(Object, 'mapper', {

    value(object: object, callback: Mapper<object>) {
        return map(object, callback);
    },
    writable: false,
    configurable: false,
    enumerable: false,

});

Object.defineProperty(Object.prototype, 'mapper', {

    value(callback: Mapper<object>) {
        return map(this, callback);
    },
    writable: false,
    configurable: false,
    enumerable: false,

});
