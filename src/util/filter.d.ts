declare type Filter<T> = (value: Readonly<T[keyof T]>, key: Readonly<keyof T>, self: Readonly<T>) => boolean;
declare type Filtered<T, F extends Filter<T>> =
    ReturnType<F> extends true ? T :
        ReturnType<F> extends false ? {} :
            Partial<T>;

declare interface ObjectConstructor {

    /**
     * Filters desired properties out of the object
     *
     * @param object Input object
     * @param callback Filter callback function
     */
    filterer<
        T extends object,
        F extends Filter<T> = Filter<T>,
    >(
        object: T,
        callback: F,
    ): Filtered<T, F>;

}

declare interface Object {

    /**
     * Filters desired properties out of the object
     *
     * @param callback Filter callback function
     */
    filterer<
        T extends this,
        F extends Filter<T> = Filter<T>,
    >(
        callback: F,
    ): Filtered<T, F>;

}
