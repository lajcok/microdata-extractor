declare type Mapper<T> = (value: T[keyof T], key: keyof T, self: T) => unknown;
declare type Mapped<T, M extends Mapper<T>> = {
    [K in keyof T]: (
            M extends (value: T[K], key: K, self: T) => infer R ? R : ReturnType<M>
        )
};

declare interface ObjectConstructor {

    /**
     * Maps all own properties to a function and
     * creates new object accordingly
     *
     * @param object Input object
     * @param callback Mapper callback function
     */
    mapper<
        T extends object,
        M extends Mapper<T> = Mapper<T>,
    >(
        object: T,
        callback: M,
    ): Mapped<T, M>;

}

declare interface Object {

    /**
     * Maps all own properties to a function and
     * creates new object accordingly
     *
     * @param callback Mapper callback function
     */
    mapper<
        T extends this,
        M extends Mapper<T> = Mapper<T>,
    >(callback: M): Mapped<T, M>;

}
