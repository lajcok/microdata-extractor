/// <reference types="./filter" />

/**
 * Filters desired properties out of the object
 *
 * @param object Input object
 * @param callback Filter callback function
 */
export function filter<
    T extends object,
    F extends Filter<T> = Filter<T>,
>(
    object: T,
    callback: Filter<T>,
): Filtered<T, F> {
    const result: any = {};
    for(const key in object) {
        if(object.hasOwnProperty(key) && callback(object[key], key, object)) {
            result[key] = object[key];
        }
    }
    return result;
}

Object.defineProperty(Object, 'filterer', {

    value(object: object, callback: Filter<object>) {
        return filter(object, callback);
    },
    writable: false,
    configurable: false,
    enumerable: false,

});

Object.defineProperty(Object.prototype, 'filterer', {

    value(callback: Filter<object>) {
        return filter(this, callback);
    },
    writable: false,
    configurable: false,
    enumerable: false,

});
