import * as React from 'react';
import InputURL from './InputURL';
import styled from 'styled-components';
import ItemsPreview from "./ResultPreview";
import logger from "../logger";
import DocumentHandler from "../class/tools/DocumentHandler";
import {Extractor, IExtractionResult} from "../class/structured-data/extraction";
import CodePreview from './CodePreview';
import 'bootstrap/dist/css/bootstrap.min.css';
import Validator from '../class/structured-data/validation';
import WebPage from '../class/tools/WebPage';

const Display = styled.div`
height: 100vh;
width: 100vw;
overflow: hidden;
`;

const Scroll = styled.div`
overflow-y: auto;
`;

interface IState {
    source: string;
    changesPending: boolean;
    result?: IExtractionResult;
}

export default class App extends React.Component<{}, IState> {

    /* Initialization */

    constructor(props: {}) {
        super(props);
        this.state = {
            source: initSource,
            changesPending: false,
        };
    }

    public componentDidMount(): void {
        this.refreshResult(initSource);
    }

    /* Component */

    public render() {
        return (
            <Display className="d-flex flex-column">
                <header className="align-self-start container-fluid bg-light border-bottom">
                    <div className="row p-2">
                        <div className="col-sm-auto mr-sm-2 text-center">
                            <h1 className="navbar-brand display-1 m-0">
                                <a href="/" className="text-dark">
                                    <span className="font-weight-bold">Microdata</span>Extractor
                                </a>
                            </h1>
                        </div>
                        <div className="col-sm">
                            <InputURL url={initUrl} onChange={this.handleUrlChange}/>
                        </div>
                    </div>
                </header>
                <div className="d-flex flex-row">
                    <div className="w-50 h-100 border-right">
                        <CodePreview onChange={this.handleSourceChange}>
                            {this.state.source}
                        </CodePreview>
                    </div>
                    <Scroll className="w-50 h-100">
                        <div id="ItemsPreview" className="p-3">
                            {this.state.changesPending &&
                            <button
                                className="btn btn-outline-primary"
                                onClick={this.handleRefresh}
                            >
                                Refresh
                            </button>}

                            {this.state.result && <ItemsPreview {...this.state.result}/>}
                        </div>
                    </Scroll>
                </div>
            </Display>
        );
    }

    /* Events */

    private readonly handleUrlChange = (url: string) => {
        WebPage.load(url)
            .then(source => {
                this.setState({source});
                this.refreshResult(source);
            });
    };

    private readonly handleSourceChange = (source: string) => {
        this.setState({
            source,
            changesPending: true,
        })
    };

    private readonly handleRefresh = () => {
        this.refreshResult(this.state.source);
    };

    private readonly refreshResult = async (source: string) => {
        const doc = new DocumentHandler(source);

        const extractor = new Extractor(doc);
        const result = extractor.extract();

        Validator.validate(result);

        logger.debug('Extractor results: ', result);
        this.setState({
            result,
            changesPending: false,
        });
    };

}


const initUrl = 'http://microdata.lajcok.cz/demo.html';
const initSource = `<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>Jiří Lajčok</title>
</head>
<body>

<section
        itemscope
        itemtype="http://schema.org/Person"
        itemid="http://lajcok.cz"
>
    <h1>Predstaveni</h1>
    <p>
        Dobry den,
        jmenuji se <span itemprop="name">Jiri Lajcok</span>,
        je mi <time datetime="1997" itemprop="birthDate">22 let</time>
        a studuji na
        <span
                itemscope
                itemtype="http://schema.org/CollegeOrUniversity"
                itemprop="affiliation"
        >
            <a href="https://www.vsb.cz/" itemprop="url">
                <span itemprop="name">VSB</span>
            </a>
        </span>.
    </p>
</section>

</body>
</html>
`;
