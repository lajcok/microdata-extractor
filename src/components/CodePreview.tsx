import * as React from 'react';
import AceEditor from 'react-ace';
import 'brace/mode/html';
import 'brace/theme/tomorrow';

interface IProps {
    children: string;
    onChange?: (source: string) => void;
}

interface IState {
    size: ISize;
}

interface ISize {
    width: string;
    height: string;
}

export default class CodePreview extends React.Component<IProps, IState> {

    /* Initialization */

    constructor(props: IProps) {
        super(props);
        this.state = {
            size: {
                width: '100%',
                height: 'calc(100vh - 57px)', // Forced value necessary to init
            }
        };
    }

    /* Lifetime */

    public componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    public readonly updateDimensions = () => {
        const wrap = document.getElementById('CodePreview');
        if (wrap) {
            this.setState({
                size: {
                    width: `${wrap.offsetWidth}px`,
                    height: `${wrap.offsetHeight}px`,
                },
            });
        }
    };

    /* Template */

    public render() {
        return (
            <div id="CodePreview" className="w-100 h-100">
                <AceEditor
                    mode="html"
                    theme="tomorrow"
                    onChange={this.props.onChange}
                    value={this.props.children}
                    {...this.state.size}
                />
            </div>
        );
    }

}
