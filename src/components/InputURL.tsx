import * as React from 'react';

interface IUrlWrap {
    url?: string,
    onChange?: (url?: string) => any,
}

export default class InputURL extends React.Component<IUrlWrap, IUrlWrap> {

    /* Initialization */

    constructor(props: IUrlWrap) {
        super(props);
        this.state = { url: props.url };
    }

    /* Template */

    public render() {
        return (
            <form onSubmit={this.onSubmit} className="input-group m-0">
                <input
                    type="url"
                    value={this.state.url}
                    onChange={this.onChange}
                    className="form-control"
                    placeholder="http://lajcok.cz/" />
                <div className="input-group-append">
                    <input type="submit" value="Load" className="btn btn-primary" />
                </div>
            </form>
        );
    }

    /* Events */

    private invokeOnChange(): void {
        if (this.props.onChange) {
            this.props.onChange(this.state.url);
        }
    }

    private onSubmit = (event: any): void => {
        this.invokeOnChange();
        event.preventDefault();
    };

    private onChange = (event: any): void => {
        this.setState({ url: event.target.value });
    };

}
