import * as React from 'react';
import {ILog} from '../class/structured-data/item';

interface IProps {
    heading?: string;
    logs: ReadonlyArray<ILog>;
}

export const Logs: React.FunctionComponent<Partial<IProps>> =
    ({heading, logs = []}) => (
        <div className="Logs card text-monospace small mb-3">
            {heading && <div className="card-header">{heading}</div>}

            <ul className="list-group list-group-flush">
                {logs.map((log, lIdx) =>
                    <li
                        className={`list-group-item list-group-item-${sortClass[log.sort]}`}
                        key={lIdx}
                    >
                        <strong>{log.phase}</strong>:{' '}
                        {log.message}
                    </li>
                )}
            </ul>
        </div>
    );

const sortClass = {
    'Notice': 'light',
    'Warning': 'warning',
    'Error': 'danger',
};
