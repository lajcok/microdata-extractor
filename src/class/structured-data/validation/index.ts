import {IValidator} from './IValidator';
import {SchemaValidator} from './SchemaValidator';
import {Schema} from './schema/Schema';
import {ValidatorContainer} from './ValidatorContainer';
import {IExtractionResult} from '../extraction';

export * from './IValidator';
export * from './SchemaValidator';
export * from './ValidatorContainer';

export default class Validator implements IValidator {

    /* Facade */

    public static validate(result: IExtractionResult) {
        Validator.validator.validate(result);
    }

    private static _validator: IValidator;
    private static get validator() {
        if (!Validator._validator) {
            Validator._validator = new Validator();
        }
        return Validator._validator;
    }

    /* Properties */

    private readonly validator: IValidator = new ValidatorContainer(
        new SchemaValidator(new Schema())
    );

    /* Validator */

    public readonly validate = (result: IExtractionResult) =>
        this.validator.validate(result);

}
