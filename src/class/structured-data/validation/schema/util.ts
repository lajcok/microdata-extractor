import {IClass, IProperty, ITerm} from './ITerm';

export const isClass = (term: ITerm): term is IClass =>
    !!term.type && term.type.includes('Class');

export const isProperty = (term: ITerm): term is IProperty =>
    !!term.type && term.type.includes('Property');
