/**
 * @see https://schema.org/docs/developers.html
 */

import schema from './schema.json';

/**
 * Every prototype must have its ID
 */
test('IDs', () => {

    expect(
        schema['@graph']
            .filter(term => typeof term['@id'] !== 'string')
            .length
    ).toEqual(0);

});

/**
 * Every type is property, class or an IRL to another term.
 * Or an array of these
 */
test('Types', () => {

    expect(
        schema['@graph']
            .map(term => term['@type'])
            .filter(term => term)
            .map(term =>
                (Array.isArray(term) ? term : [term])
            )
            .filter(term =>
                term.filter((type: string) => !(
                    type === 'rdf:Property' ||
                    type === 'rdfs:Class' ||
                    type.startsWith('http://schema.org/')
                )).length !== 0
            )
            .length
    ).toEqual(0);

});

/**
 * Only classes have subclasses specified
 */
test('Subclass', () => {

    expect(
        schema['@graph']
            .filter(term =>
                term['rdfs:subClassOf']
                && !(Array.isArray(term['@type'])
                    ? term['@type']
                    : [term['@type']]
                ).includes('rdfs:Class')
            ).length
    ).toEqual(0);

});

/**
 * All data types are classes at the same time
 */
test('Data type', () => {

    expect(
        schema['@graph']
            .filter(term => {
                const type = (Array.isArray(term['@type'])
                        ? term['@type']
                        : [term['@type']]
                );
                return type.includes('http://schema.org/DataType')
                    && !type.includes('rdfs:Class');
            })
            .length
    ).toEqual(0);

});

/**
 * Checks that domain specification is always specified on property
 */
test('domainIncludes', () => {

    // expect(
        schema['@graph']
            .filter(term => {
                const type = term['@type'];

                const hasDomain = term['http://schema.org/domainIncludes'];
                const isProperty = (Array.isArray(type) ? type : [type])
                    .includes('rdf:Property');

                return hasDomain && !isProperty;
            })
            .map(term => {
                console.log(term);
                return term;
            })
            // .length
    // ).toEqual(0);

});
