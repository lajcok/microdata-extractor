import {IClass, IProperty, ITerm} from './ITerm';
import {ISchema} from './ISchema';

export class TermLoader implements ITerm, IClass, IProperty {

    /* Properties */

    private _data: ITerm & IClass & IProperty;

    /* Initialization */

    constructor(
        public readonly id: string,
        private readonly schema: ISchema,
    ) {}

    /* Term */

    public get type() {
        return this.data.type;
    }

    public get label() {
        return this.data.label;
    }

    public get comment() {
        return this.data.comment;
    }

    public get supersededBy() {
        return this.data.supersededBy;
    }

    public get subClassOf() {
        return this.data.subClassOf;
    }

    public get rangeIncludes() {
        return this.data.rangeIncludes;
    }

    public get domainIncludes() {
        return this.data.domainIncludes;
    }

    /* Helpers */

    private get data() {
        if (!this._data) {
            this._data = this.schema.get(this.id) || {id: this.id};
        }
        return this._data;
    }

}