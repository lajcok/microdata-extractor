export * from './ISchema';
export * from './ITerm';
export * from './util';

import {Schema} from './Schema';
export default Schema;
