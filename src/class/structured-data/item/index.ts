import * as Spec from './spec';

export * from "./IItem";
export * from "./IMeta";
export * from "./ILog";
export {Spec};
