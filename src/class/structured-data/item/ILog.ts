import {ITerm} from '../validation/schema';

/**
 * Log entry
 */
export interface ILog {

    /**
     * Level of relevance of the entry
     *
     * - `Notice` -- Observation worth noticing but is completely valid
     * - `Warning` -- Problem found which can be skipped and ignored
     * - `Error` -- Strong mistake which causes unexpected behavior
     */
    sort: 'Notice' | 'Warning' | 'Error';

    /**
     * The message
     */
    message: string;

    /**
     * Phase in which the log has been emitted
     */
    phase: 'Parsing' | 'Extraction' | 'Semantics';

    /**
     * Reference to an element
     */
    element?: Element;

    /**
     * Reference to a term
     * TODO Need this tho?
     */
    term?: ITerm;

}
