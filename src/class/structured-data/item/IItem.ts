import {IMeta} from './IMeta';
import {Spec} from './';

// TODO Add tools for term mapping to item, property and values

/**
 * And extension of specification item
 */
export interface IItem extends Spec.IItem {

    /**
     * Additional metadata about the item
     */
    meta: IMeta;

    /**
     * Property wrappers containing metadata
     */
    propMeta: IMetaProperties<IItem>;

    /**
     * Overridden properties
     */
    properties: Spec.IProperties<IItem>;

}

/**
 * Structure holding property wrappers
 */
export interface IMetaProperties<T extends Spec.IItem = IItem> {
    // TODO Replace array by another container?
    [name: string]: Array<IMetaProperty<T>>;
}

/**
 * Property wrapper adding meta
 */
export interface IMetaProperty<T extends Spec.IItem = IItem> {

    /**
     * The name of the property
     */
    name: string;

    /**
     * The value of the property
     */
    value: Spec.Value<T>;

    /**
     * Additional metadata about the property
     */
    meta: IMeta;

}
