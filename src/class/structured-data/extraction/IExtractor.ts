import {IItem, ILog, Spec} from '../item';

/**
 * An interface to implement structured data extractor
 * most likely from HTML documents
 */
export interface IExtractor<T extends Spec.IItem = IItem> {

    /**
     * Get extracted top-level items
     */
    extract(): IExtractionResult<T>;

}

/**
 * An extraction result object interface
 */
export interface IExtractionResult<T extends Spec.IItem = IItem> {

    /**
     * Valid structured data found in the document
     */
    items: T[];

    /**
     * Log entries, errors etc.
     */
    logs: ILog[];

}
