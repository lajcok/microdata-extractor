import {IExtractionResult, IExtractor} from "./IExtractor";

export class ExtractorContainer implements IExtractor {

    /* Properties */

    private readonly extractors: IExtractor[];

    /* Initialization */

    constructor(...extractor: IExtractor[]) {
        this.extractors = extractor.slice();
    }

    /* Manipulation */

    public add(...extractor: IExtractor[]) {
        this.extractors.push(...extractor);
        return this;
    }

    /* Extraction */

    public readonly extract = (): IExtractionResult => {
        const results: IExtractionResult = {items: [], logs: []};
        for(const result of this.extractors.map(e => e.extract())) {
            results.items.push(...result.items);
            results.logs.push(...result.logs);
        }
        return results;
    };

}
