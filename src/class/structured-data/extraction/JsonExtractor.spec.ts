import {IExtractionResult} from "./IExtractor";
import DocumentHandler from "../../tools/DocumentHandler";
import {JsonExtractor} from "./JsonExtractor";
import {IItem} from "../item/spec";

function testOutput(source: string, expected: Pick<IExtractionResult<IItem>, 'items'>) {
    const doc = new DocumentHandler(source, 'http://example.com');
    const res = new JsonExtractor(doc).extract();

    expect(res).toMatchObject(expected);
}

/**
 * Parsing structured data
 */
test('Parsing data', () => {

    const source = `
<div>
    <span>propValue</span>
    <span>propValue2</span>
</div>
<script type="application/ld+json"></script>
<script type="application/ld+json">
"this is not an object!"
</script>
<script type="application/ld+json">
[
    "not an object",
    null,
    [{}],
    {}
]
</script>
`;

    testOutput(source, {
        items: [
            {
                type: [],
                properties: {},
            },
            {
                type: [],
                properties: {},
            },
        ],
    });

});

/**
 * @see https://developers.google.com/search/docs/guides/intro-structured-data
 */
test('Simple example', () => {

    const source = `
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": "http://www.example.com",
  "name": "Unlimited Ball Bearings Corp."
}
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://schema.org/Organization'],
                properties: {
                    'https://schema.org/url': ['http://www.example.com'],
                    'https://schema.org/name': ['Unlimited Ball Bearings Corp.'],
                },
            },
        ],
    });

});

/**
 * @see https://developers.google.com/search/docs/guides/intro-structured-data
 */
test('Nested items', () => {

    const source = `
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": "http://www.example.com",
  "name": "Unlimited Ball Bearings Corp.",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+1-401-555-1212",
    "contactType": "Customer service"
  }
}
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://schema.org/Organization'],
                properties: {
                    'https://schema.org/url': ['http://www.example.com'],
                    'https://schema.org/name': ['Unlimited Ball Bearings Corp.'],
                    'https://schema.org/contactPoint': [
                        {
                            type: ["https://schema.org/ContactPoint"],
                            properties: {
                                'https://schema.org/telephone': ["+1-401-555-1212"],
                                'https://schema.org/contactType': ["Customer service"],
                            },
                        },
                    ],
                },
            },
        ],
    });

});

/**
 * @see https://developers.google.com/search/docs/guides/intro-structured-data
 */
test('Multiple values', () => {

    const source = `
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": ["http://www.example.com", "http://www.example.com/another"],
  "name": "Unlimited Ball Bearings Corp."
}
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://schema.org/Organization'],
                properties: {
                    'https://schema.org/url': ['http://www.example.com', 'http://www.example.com/another'],
                    'https://schema.org/name': ['Unlimited Ball Bearings Corp.'],
                },
            },
        ],
    });

});

/**
 * @see https://developers.google.com/search/docs/guides/intro-structured-data
 */
test('Multiple items', () => {

    const source = `
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": "http://www.example.com",
  "name": "Unlimited Ball Bearings Corp.",
  "contactPoint": [
      {
        "@type": "ContactPoint",
        "telephone": "+1-401-555-1212",
        "contactType": "Customer service"
      },
      {
        "@type": "ContactPoint",
        "telephone": "123456789",
        "contactType": "Fictional"
      }
  ]
}
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://schema.org/Organization'],
                properties: {
                    'https://schema.org/url': ['http://www.example.com'],
                    'https://schema.org/name': ['Unlimited Ball Bearings Corp.'],
                    'https://schema.org/contactPoint': [
                        {
                            type: ["https://schema.org/ContactPoint"],
                            properties: {
                                'https://schema.org/telephone': ["+1-401-555-1212"],
                                'https://schema.org/contactType': ["Customer service"],
                            },
                        },
                        {
                            type: ["https://schema.org/ContactPoint"],
                            properties: {
                                'https://schema.org/telephone': ["123456789"],
                                'https://schema.org/contactType': ["Fictional"],
                            },
                        },
                    ],
                },
            },
        ],
    });

});

/**
 * Skipping and trimming values, other quirks
 */
test('Empty, trimmed', () => {

    const source = `
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Person",
  "url": [
    "   http://www.example.com    \\n   ",
    "",
    null,
    false
  ]
}
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['https://schema.org/Person'],
                properties: {
                    'https://schema.org/url': ['http://www.example.com', 'false'],
                },
            },
        ],
    });

});

/**
 * Dealing with malformed context
 */
test('Malformed context', () => {

    const source = `
<script type="application/ld+json">
[
    {
      "@context": "schema.org",
      "@type": "Person",
      "name": "John Doe" 
    },
    {
      "@context": "http://schema.org",
      "@type": "http://example.org/Person",
      "name": "John Doe"
    }
]
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['Person'],
                properties: {
                    'name': ['John Doe'],
                },
            },
            {
                type: ['http://example.org/Person'],
                properties: {
                    'http://schema.org/name': ['John Doe'],
                },
            },
        ],
    });

});

/**
 * Dealing with hash context
 */
test('Hash context', () => {

    const source = `
<script type="application/ld+json">
[
    {
      "@context": "http://example.org/whatever#",
      "@type": "Person",
      "name": "John Doe"
    }
]
</script>
`;

    testOutput(source, {
        items: [
            {
                type: ['http://example.org/whatever#Person'],
                properties: {
                    'http://example.org/whatever#name': ['John Doe'],
                },
            },
        ],
    });

});
