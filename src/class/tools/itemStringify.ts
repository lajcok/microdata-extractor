import {IItem, ILog} from '../structured-data/item';
import stringify from './CsvStringify';
import {isItem} from '../structured-data/item/spec';
import {IExtractionResult} from '../structured-data/extraction';

export function itemStringify(item: IItem, logs: ILog[] = [], nr: number = 1) {
    const lines: Array<{
        item: string,
        type: string,
        id: string,
        name: string,
        value: string,
        tech: string,
        logCat: string,
        logPhase: string
        logMessage: string,
    }> = [];
    const pending: IItem[] = [item];
    const itemsSer: IItem[] = [];

    logs.forEach(l => {
            lines.push({
                item: '', type: '', id: '', name: '', value: '', tech: '',
                logCat: l.sort,
                logPhase: l.phase,
                logMessage: l.message,
            });
        });

    while(pending.length) {
        const it = pending.shift()!;
        itemsSer.push(it);
        for (const name in it.propMeta) {
            if (!it.propMeta.hasOwnProperty(name)) {
                continue;
            }
            it.propMeta[name].forEach(prop => {
                if(isItem(prop.value)) {
                    pending.push(prop.value);
                }
            });
        }
    }


    for (const it of itemsSer) {
        const type = it.type
            .map(t => unSchemaOrg(t))
            .join(', ');
        const id = it.id || '';

        /* Item logs */
        lines.push({
            item: `Item[${nr}.${itemsSer.indexOf(it)+1}]`,
            type,
            id,
            name: '', value: '',
            tech: itemsSer.indexOf(it) === 0 ? it.meta.type : '',
            logCat: '', logPhase: '', logMessage: '',
        });
        it.meta.logs
            .forEach(l => {
                lines.push({
                    item: '', type: '', id: '', name: '', value: '', tech: '',
                    logCat: l.sort,
                    logPhase: l.phase,
                    logMessage: l.message,
                });
            });

        for (const name in it.propMeta) {
            if(!it.propMeta.hasOwnProperty(name)) {
                continue;
            }

            it.propMeta[name].forEach(prop => {
                const value = isItem(prop.value)
                    ? `Item[${nr}.${itemsSer.indexOf(prop.value)+1}]`
                    : prop.value;

                /* Property */
                lines.push({
                    item: '', type: '', id: '',
                    name: unSchemaOrg(prop.name),
                    value,
                    tech: '', logCat: '', logPhase: '', logMessage: '',
                });
                prop.meta.logs
                    .forEach(l => {
                        lines.push({
                            item: '', type: '', id: '', name: '', value: '', tech: '',
                            logCat: l.sort,
                            logPhase: l.phase,
                            logMessage: l.message,
                        });
                    });


            });
        }
    }
    return stringify(lines, nr === 1);
}

export function resultStringify(result: IExtractionResult) {
    return result.items
        .map((item, idx) => itemStringify(item, idx === 0 ? result.logs : [], idx+1))
        .join('\r\n');
}

const prefix = 'http://schema.org/';
const unSchemaOrg = (type: string) =>
    type.startsWith(prefix) && type.length > prefix.length
        ? type.substr(prefix.length)
        : type;
