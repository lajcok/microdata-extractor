import WebPage from "./WebPage";

interface IDocumentHandler {

    url?: string;
    source: string;
    document: DocumentFragment;

}

export default class DocumentHandler implements Readonly<IDocumentHandler> {

    /* Static */

    public static readonly fromURL = (url: string): Promise<DocumentHandler> =>
        WebPage.load(url).then(source => new DocumentHandler(source, url));

    /* Properties */

    public readonly document: DocumentFragment;

    /* Initialization */

    constructor(
        public readonly source: string,
        public readonly url?: string,
    ) {
        this.document = new DOMParser().parseFromString(source, 'text/html');
    }

}
