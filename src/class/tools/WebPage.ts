import axios from 'axios';

interface IWebPage {

    readonly uri: string;

    load(): Promise<string>;

}

export default class WebPage implements IWebPage {

    /* Static */

    public static readonly load = (uri: string) => new WebPage(uri).load();

    /* Initialization */

    constructor(public readonly uri: string) {
    }

    /* Loader */

    public readonly load = () =>
        axios.get('curl.php?u=' + encodeURIComponent(this.uri))
            .then(response => String(response.data));

}
