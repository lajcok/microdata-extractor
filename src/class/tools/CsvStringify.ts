/**
 * @see https://stackoverflow.com/questions/8847766/how-to-convert-json-to-csv-format-and-store-in-a-variable
 * @param data
 * @param includeHeader
 * @param del
 */
export default function stringify(data: Array<{[key: string]: string}>, includeHeader: boolean = true, del: string = ',') {
    const header = data.length ? Object.keys(data[0]) : [];
    const csv = data
        .map(row =>
            header
                .map(fieldName =>
                    JSON.stringify(
                        row[fieldName],
                        (key, value) => value === null ? '' : value)
                )
                .join(del)
        );

    if(includeHeader) {
        csv.unshift(header.join(del));
    }

    return csv.join('\r\n');
}
