// const enabled = 'dev' === (process.env.NODE_ENV||'').substr(0, 3);
const enabled = true;

const logger = enabled ? console : new Proxy( {}, {
    get: (target: any, name: string) => name !== 'memory' ? () => void 0 : undefined
} );

export default logger as Console;
