<?php

$url = filter_var(@$_GET['u'], FILTER_VALIDATE_URL);

if ($url && substr($url, 0, 4) === 'http') {
    @readfile($url);
} else {
    http_response_code(404);
    exit('URL is not valid');
}
